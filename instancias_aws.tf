terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  shared_credentials_files = [ "./cred" ]
  region = var.config
}

resource "aws_instance" "consul" {
  ami = "ami-08d4ac5b634553e16"
  instance_type = "t2.nano"
  key_name = "hashi_estudos"
  vpc_security_group_ids = [data.aws_security_group.wizard.id]
  ebs_block_device {
    device_name = "/dev/sdg"
    volume_size = "10"
    volume_type = "gp2"
  }

  tags = {
    Name = "consul-server"
  }
}

resource "aws_instance" "vault" {
  ami = "ami-08d4ac5b634553e16"
  instance_type = "t2.nano"
  count = 3
  key_name = "hashi_estudos"
  vpc_security_group_ids = [data.aws_security_group.wizard.id]
  ebs_block_device {
    device_name = "/dev/sdf"
    volume_size = "15"
    volume_type = "gp2"
  }

  tags = {
    Name = "vault${count.index}"
  }

}

data "aws_security_group" "wizard" {
  id = "sg-01398b94e82ae8d75"
}
